<?php
require 'vendor/autoload.php';

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Src\Lib\Config;

$isDevMode = true;
$proxyDir = null;
$cache = null;
$useSimpleAnnotationReader = false;
$config = Setup::createAnnotationMetadataConfiguration(array(__DIR__."/src/Models"), $isDevMode, $proxyDir, $cache, $useSimpleAnnotationReader);

$conn = array (
    'dbname' => Config::get('DB_DATABASE', ''),
    'user' => Config::get('DB_USERNAME', ''),
    'password' => Config::get('DB_PASSWORD', ''),
    'host' => Config::get('DB_HOST', ''),
    'port' => Config::get('DB_PORT', ''),
    'driver' => Config::get('DB_DRIVER', ''),
);

$entityManager = EntityManager::create($conn, $config);

