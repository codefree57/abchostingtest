<?php
return [
    'LOG_PATH' => __DIR__ . '/logs',
    'DB_DRIVER' => 'pdo_mysql',
    'DB_HOST' => 'localhost',
    'DB_PORT' => '3306',
    'DB_DATABASE' => 'codefree57',
    'DB_USERNAME' => 'root',
    'DB_PASSWORD' => 'P@ssw0rd',
];
