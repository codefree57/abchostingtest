<?php

namespace Src;

use Src\Lib\Router;
use Src\Lib\Request;
use Src\Lib\Response;
use Src\Controller\ProductController;
use Src\Controller\CartController;
use Src\Lib\Logger;
use Exception;

class App
{
    /**
     *
     * Execute Log
     *
     */
    public static function log()
    {
        Logger::enableSystemLogs();
    }

    /**
     *
     * Execute defined Routes
     *
     */
    public static function route()
    {
        Router::get('/', function (Request $req, Response $res) {
            (new ProductController)->index();
        });

        Router::get('/cart', function (Request $req, Response $res) {
            (new CartController)->index();
        });

        Router::post('/api/cart/store', function (Request $req, Response $res) {
            $result = (new CartController)->store($req->getJSON());
            return $res->status(200)->toJSON($result);
        });

        Router::post('/api/cart/update/([0-9]*)', function (Request $req, Response $res) {
            $result = (new CartController)->update($req->getJSON(), $req->params[0]);
            return $res->status(200)->toJSON($result);
        });

        Router::post('/api/cart/pay', function (Request $req, Response $res) {
            $result = (new CartController)->pay($req->getJSON());
            return $res->status(200)->toJSON($result);
        });

        Router::delete('/api/cart/delete/([0-9]*)', function (Request $req, Response $res) {
            $result = (new CartController)->delete($req->params[0]);
            $res->status(200)->toJSON($result);
        });

        Router::post('/api/product/rate', function (Request $req, Response $res) {
            $result = (new ProductController)->rate($req->getJSON());
            return $res->status(200)->toJSON($result);
        });

        Router::get('/api/product/avg/([0-9]*)', function (Request $req, Response $res) {
            $result = (new ProductController)->calculateAvg($req->params[0]);
            return $res->status(200)->toJSON($result);
        });

        // If user send a request to a route that is not defined
        try {
            throw new \Exception('resource not found');
        } catch (Exception $e) {
            (new Response())->status(404)->toJSON([
                'error' => 1,
                'message' =>  $e->getMessage(),
                'status' => 404
            ]);
        }
    }
}
