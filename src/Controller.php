<?php
namespace Src;

use Src\Lib\Session;

class Controller
{
    protected $session;
    protected $compact;

    /**
     *
     *  constructor where to define session and compact
     *
     */
    public function __construct()
    {
        $this->session = new Session();
        $this->compact['session'] = $this->session;
        if ($this->session->get('wallet') === false)
        {
            $this->session->set('wallet', 100);
        }

        if ($this->session->get('items') === false)
        {
            $this->session->set('items', []);
        }

        if ($this->session->get('items_count') === false)
        {
            $this->session->set('items_count', 0);
        }
        else
        {
            $count = 0;
            $items = $this->session->get('items');
            foreach($items as $key => $item)
            {
                $count += $item->quantity;
            }
            $this->session->set('items_count', $count);
        }

        if ($this->session->get('rate') === false)
        {
            $this->session->set('rate', []);
        }
    }
}
