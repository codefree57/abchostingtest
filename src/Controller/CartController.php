<?php
namespace Src\Controller;

use Src\Lib\View;
use Src\Controller;

class CartController extends Controller
{
    /**
     * CartController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     * render cart template
     */
    public function index()
    {
        (new view('Cart'))->assign('compact', $this->compact);
    }

    /**
     *
     * function to store items in cart
     *
     * @param $input
     * @return array
     */
    public function store ($input)
    {
        $items = $this->session->get('items');

        if (array_key_exists($input->id, $items))
        {
            $items[$input->id]->quantity += 1;
        }
        else
        {
            $items[$input->id] = $input;
            $items[$input->id]->quantity = 1;
        }

        $this->session->set('items', $items);


        return [
            'status' => 'ok'
        ];
    }

    /**
     *
     * function to update items in cart
     *
     * @param $input
     * @return array
     */
    public function update ($input, $id)
    {
        $items = $this->session->get('items');
        $items[$id]->quantity = $input->quantity;
        $this->session->set('items', $items);

        return [
            'status' => 'ok'
        ];
    }

    /**
     * function to validate payment
     *
     * @param $input
     * @return mixed
     */
    public function pay($input)
    {
        $items = $this->session->get('items');

        $subTotal = 0;
        foreach ($items as $key => $item)
        {
            $subTotal += $item->price * $item->quantity;
        }

        $total = $subTotal;
        if ($input->shipping_option == 'ups')
        {
            $total += 5;
        }

        $wallet = $this->session->get('wallet');
        if ($wallet < $total)
        {
            return [
                'status' => 'nok',
            ];
        }

        $this->session->set('items', []);
        $this->session->set('items_count', 0);
        $this->session->set('wallet', $wallet - $total);

        return [
            'status' => 'ok',
        ];
    }

    /**
     * function to delete items from cart
     *
     * @param $id
     * @return string[]
     *
     */
    public function delete($id)
    {
        $items = $this->session->get('items');
        unset($items[$id]);

        $this->session->set('items', $items);


        return ['status' => 'ok', 'items' => $items];
    }
}
