<?php
namespace Src\Controller;

use Src\Lib\View;
use Src\Controller;
use Src\GateWay\ProductGateWay;

class ProductController extends Controller
{
    /**
     * ProductController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     * render product template
     */
    public function index ()
    {
        $this->compact['products'] = (new ProductGateWay())->all();

        (new view('Product'))->assign('compact', $this->compact);
    }

    /**
     * function to rate a product
     *
     * @param $input
     * @return string[]
     */
    public function rate($input)
    {

        if (array_key_exists($input->product_id, $this->session->get('rate')))
        {
            return ['status' => 'nok'];
        }

        $result = (new ProductGateWay())->rate($input);
        $rate = $this->session->get('rate');
        $rate[$input->product_id] = 'yes';
        $this->session->set('rate', $rate);
        return ['status' => 'ok'];
    }

    /**
     * Calculate rating for one product
     *
     * @param $id
     * @return mixed
     */
    public function calculateAvg($id)
    {
        return (new ProductGateWay())->calculateAvg($id);
    }
}