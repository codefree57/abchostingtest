<?php
namespace Src;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Src\Lib\Config;

class GateWay
{
    public $entityManager;

    /**
     *
     *  constructor to define $entityManager that will be used in children classes
     *
     */
    public function __construct()
    {
        $isDevMode = true;
        $proxyDir = null;
        $cache = null;
        $useSimpleAnnotationReader = false;
        $config = Setup::createAnnotationMetadataConfiguration(array(__DIR__."/src/Model"), $isDevMode, $proxyDir, $cache, $useSimpleAnnotationReader);

        $conn = array (
            'dbname' => Config::get('DB_DATABASE', ''),
            'user' => Config::get('DB_USERNAME', ''),
            'password' => Config::get('DB_PASSWORD', ''),
            'host' => Config::get('DB_HOST', ''),
            'port' => Config::get('DB_PORT', ''),
            'driver' => Config::get('DB_DRIVER', ''),
        );

        $this->entityManager = EntityManager::create($conn, $config);
    }
}
