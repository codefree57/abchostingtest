<?php
namespace Src\GateWay;

use Src\Models\Product;
use Src\Models\Rate;
use Src\GateWay;

class ProductGateWay extends GateWay {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     *  function to convert doctrine result to an array
     *
     * @param Product $product
     * @return array
     */
    private function toArray(Product $product)
    {
        return array(
            'id' => $product->getId(),
            'name' => $product->getName(),
            'price' => $product->getPrice(),
            'image' => $product->getImage(),
        );
    }

    /**
     *
     *  function to get all Product record
     *  SELECT p.id, p.name, p.price, p.image, AVG(r.value) AS avg
     *  FROM products p LEFT OUTER JOIN rates r ON r.product_id = p.id GROUP BY(p.name);
     *
     * @return array
     */
    public function all()
    {
        $sql = '
             SELECT p.id, p.name, p.price, p.image, AVG(r.value) AS avg
             FROM products p LEFT OUTER JOIN rates r ON r.product_id = p.id GROUP BY(p.name);
        ';

        $stmt = $this->entityManager->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function rate($input)
    {
        $product = $this->find($input->product_id);

        if ($product === null)
        {
            return false;
        }

        $rate = new Rate();
        $rate->setValue($input->value);
        $rate->setProduct($product);
        $this->entityManager->persist($rate);
        $this->entityManager->flush();
        return true;
    }

    /**
     *
     *  function to get one record by id
     *
     * @param int $id id to search
     * @return array
     */
    public function find(int $id)
    {
        return $this->entityManager->find(Product::class, $id);
    }

    /**
     *
     *  function to get records by a part of first_name or last_name
     *
     * @param int $id
     * @return array
     */
    public function calculateAvg(int $id)
    {
        return $this->entityManager->getRepository(Rate::class)->createQueryBuilder('r')
            ->select("AVG(r.value)")
            ->where('r.product  = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getArrayResult();
    }

    /**
     *
     *  function to insert a record
     *
     * @param mixed $input json decode
     * @return array
     */
    public function insert($input)
    {
//        $validator = new PhoneBookValidator();
//        $validator->store($input);
//
//        if ($validator->getError() !== null) {
//            return $this->errorMessage($validator->getError());
//        }
//
//        $phoneBook = new PhoneBook();
//        $phoneBook->setFirstName($input->first_name);
//        $phoneBook->setLastName($input->last_name);
//        $phoneBook->setPhoneNumber($input->phone_number);
//        $phoneBook->setCountryCode($input->country_code);
//        $phoneBook->setTimezoneName($input->timezone_name);
//
//        $this->entityManager->persist($phoneBook);
//        $this->entityManager->flush();
//
//        $data = $this->toArray($phoneBook);
//
//        return $this->okMessage($data, 'contact has been registered');
    }

    /**
     *
     *  function to update a given record
     *
     * @param mixed $input json decode
     * @param int $id
     * @return array
     */
    public function update($input, int $id)
    {
//        $phoneBook = $this->entityManager->find(PhoneBook::class, $id);
//
//        if ($phoneBook === null) {
//            return $this->recordNotFound();
//        }
//
//        $validator = new PhoneBookValidator();
//        $validator->update($input);
//
//        if ($validator->getError() !== null) {
//            return $this->errorMessage($validator->getError());
//        }
//
//        if (array_key_exists('first_name', $input)) {
//            $phoneBook->setFirstName($input->first_name);
//        }
//
//        if (array_key_exists('last_name', $input)) {
//            $phoneBook->setLastName($input->last_name);
//        }
//
//        if (array_key_exists('phone_number', $input)) {
//            $phoneBook->setPhoneNumber($input->phone_number);
//        }
//
//        if (array_key_exists('country_code', $input)) {
//            $phoneBook->setCountryCode($input->country_code);
//        }
//
//        if (array_key_exists('timezone_name', $input)) {
//            $phoneBook->setTimezoneName($input->timezone_name);
//        }
//
//        $this->entityManager->flush();
//        $data = $this->toArray($phoneBook);
//
//        return $this->okMessage($data, 'record has been updated');
    }

    /**
     *
     *  function to create a record
     *
     * @param int $id
     * @return array
     */
    public function delete(int $id)
    {
//        $phoneBook = $this->entityManager->find(PhoneBook::class, $id);
//
//        if ($phoneBook === null) {
//            return $this->recordNotFound();
//        }
//
//        $data = $this->toArray($phoneBook);
//        $this->entityManager->remove($phoneBook);
//        $this->entityManager->flush();
//
//        return $this->okMessage($data, 'record with '.$id.' has been deleted');
    }
}
