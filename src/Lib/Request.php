<?php

namespace Src\Lib;

class Request
{
    public $params;
    public $reqMethod;
    public $contentType;

    /**
     *
     * Constructor function
     *
     * @param array $params params in url'
     */
    public function __construct($params = [])
    {
        $this->params = $params;
        $this->reqMethod = trim($_SERVER['REQUEST_METHOD']);
        $this->contentType = !empty($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
    }

    /**
     *
     *  function to get body
     *
     * @return array|string
     */
    public function getBody()
    {
        if ($this->reqMethod !== 'POST' && $this->reqMethod !== 'PUT')
        {
            return '';
        }

        $body = [];
        foreach ($_POST as $key => $value) {
            $body[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);
        }

        return $body;
    }

    /**
     *
     *  function to get json
     *
     * @return array|object(decode json)
     */
    public function getJSON()
    {
        if ($this->reqMethod !== 'POST' && $this->reqMethod !== 'PUT')
        {
            return [];
        }

        if (strcasecmp($this->contentType, 'application/json') !== 0)
        {
            return [];
        }

        // Receive the RAW post data.
        $content = trim(file_get_contents("php://input"));
        return json_decode($content);
    }
}
