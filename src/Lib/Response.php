<?php

namespace Src\Lib;

class Response
{
    private $status = 200;

    /**
     *
     *  function to define status default status is 200
     *
     * @param int $code status code
     * @return Response
     */
    public function status(int $code)
    {
        $this->status = $code;
        return $this;
    }

    /**
     *
     *  function to convert array to json
     *
     * @param array $data
     */
    public function toJSON($data = [])
    {
        http_response_code($this->status);
        header('Content-Type: application/json');
        echo json_encode($data);
    }
}
