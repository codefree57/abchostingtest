<?php
namespace Src\Lib;

class Router
{
    /**
     *
     * Define Get method
     *
     * @param string $route the name of the route
     * @param function void $callback the to call from controller
     */
    public static function get(string $route, $callback)
    {
        if (strcasecmp($_SERVER['REQUEST_METHOD'], 'GET') !== 0)
        {
            return;
        }

        self::on($route, $callback);
    }

    /**
     *
     * Define Post method
     *
     * @param string $route the name of the route
     * @param function void $callback the to call from controller
     */
    public static function post(string $route, $callback)
    {
        if (strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') !== 0)
        {
            return;
        }

        self::on($route, $callback);
    }

    /**
     *
     * Define Put method
     *
     * @param string $route the name of the route
     * @param function void $callback the to call from controller
     */
    public static function put(string $route, $callback)
    {
        if (strcasecmp($_SERVER['REQUEST_METHOD'], 'PUT') !== 0)
        {
            return;
        }

        self::on($route, $callback);
    }

    /**
     *
     * Define Delete method
     *
     * @param string $route the name of the route
     * @param function void $callback the to call from controller
     */
    public static function delete(string $route, $callback)
    {
        if (strcasecmp($_SERVER['REQUEST_METHOD'], 'DELETE') !== 0)
        {
            return;
        }

        self::on($route, $callback);
    }

    /**
     *
     * Search and execute the right route
     *
     * @param string $regex the name of the route to check
     * @param function void $callback function to call from controller
     */
    public static function on(string $regex, $callback)
    {
        $params = $_SERVER['REQUEST_URI'];
        $params = (stripos($params, '/') !== 0) ? '/' . $params : $params;
        $regex = str_replace('/', '\/', $regex);
        $is_match = preg_match('/^' . ($regex) . '$/', $params, $matches, PREG_OFFSET_CAPTURE);

        if ($is_match)
        {
            // first value is normally the route, lets remove it
            array_shift($matches);
            // Get the matches as parameters
            $params = array_map(function ($param) {
                return $param[0];
            }, $matches);
            $callback(new Request($params), new Response());
            exit();
        }
    }
}
