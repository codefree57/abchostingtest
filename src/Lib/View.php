<?php

namespace Src\Lib;

use Exception;

class View
{
    private $data = array();
    private $render = false;

    /**
     * View constructor.
     * @param $template
     * @throws Exception
     */
    public function __construct($template)
    {
        try
        {
            $file = __DIR__ . '/../../templates/' . strtolower($template) . '.php';
            if (file_exists($file))
            {
                $this->render = $file;
            }
            else
            {
                throw new \Exception('Template ' . $template . ' not found!');
            }
        }
        catch (customException $e)
        {
            echo $e->errorMessage();
        }
    }

    /**
     * View Destruct
     */
    public function __destruct()
    {
        extract($this->data);
        include($this->render);
    }

    /**
     * @param $variable
     * @param $value
     */
    public function assign($variable, $value)
    {
        $this->data[$variable] = $value;
    }
}
