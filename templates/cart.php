<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Card</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>

<body>
<style>
    .bloc_left_price {
        color: #c01508;
        text-align: center;
        font-weight: bold;
        font-size: 150%;
    }
    .category_block li:hover {
        background-color: #007bff;
    }
    .category_block li:hover a {
        color: #ffffff;
    }
    .category_block li a {
        color: #343a40;
    }
    .add_to_cart_block .price {
        color: #c01508;
        text-align: center;
        font-weight: bold;
        font-size: 200%;
        margin-bottom: 0;
    }
    .add_to_cart_block .price_discounted {
        color: #343a40;
        text-align: center;
        text-decoration: line-through;
        font-size: 140%;
    }
    .product_rassurance {
        padding: 10px;
        margin-top: 15px;
        background: #ffffff;
        border: 1px solid #6c757d;
        color: #6c757d;
    }
    .product_rassurance .list-inline {
        margin-bottom: 0;
        text-transform: uppercase;
        text-align: center;
    }
    .product_rassurance .list-inline li:hover {
        color: #343a40;
    }
    .reviews_product .fa-star {
        color: gold;
    }
    .pagination {
        margin-top: 20px;
    }
    footer {
        background: #343a40;
        padding: 40px;
    }
    footer a {
        color: #f8f9fa!important
    }
</style>

<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="container">
        <a class="navbar-brand" href="/">AbcTest</a>

        <div class="collapse navbar-collapse justify-content-end" id="navbarsExampleDefault">
            <a class="btn btn-success btn-sm ml-3" href="#1">
                <i class="fa fa-money"></i> <?php echo $compact['session']->get('wallet') ?>
                <span class="badge badge-light">USD</span>
            </a>
            <a class="btn btn-success btn-sm ml-3" href="/cart">
                <i class="fa fa-shopping-cart"></i> Cart
                <span class="badge badge-light" id="cart"> <?php echo $compact['session']->get('items_count') ?> </span>
            </a>
        </div>
    </div>
</nav>

<div class="container mb-4">
    <div class="row">
        <div class="col-12">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col"> </th>
                        <th scope="col">Product</th>
                        <th scope="col" class="text-center">Quantity</th>
                        <th scope="col" class="text-right">Price</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    $items = $compact['session']->get('items');
                    $subTotal = 0;
                    $total = 0;
                    foreach ($items as $key => $item) {

                        $id = $item->id;
                        $name = $item->name;
                        $quantity = $item->quantity;
                        $price = $item->price * $quantity;
                        $image = $item->image;
                        $subTotal += $price;
                        echo <<<HEREDOC
                    <tr>
                        <td><img src="/$image"  height="50" /> </td>
                        <td>$name</td>
                        <td class="text-center"><input class="quantity" type="number" 
                                                       value="$quantity" min="1" step="1" 
                                                       data-id="$id"/></td>
                        <td class="text-right">$price USD</td>
                        <td class="text-right"><button class="delete-item btn btn-sm btn-danger" data-id="$id"><i class="fa fa-trash"></i> </button> </td>
                    </tr>
HEREDOC;

                    }
                    ?>

                    <tr>
                        <td></td>
                        <td></td>
                        <td>Sub-Total</td>
                        <td class="text-right"> <span id="sub-total"><?php echo $subTotal ?></span> USD </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>Shipping</td>
                        <td class="text-right" >
                            <select id ="shipping-option" class="form-control">
                                <option value="null">N/A</option>
                                <option value="pickup">Pickup 0 USD</option>
                                <option value="ups">UPS 5 USD</option>
                            </select>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><strong>Total</strong></td>
                        <td class="text-right"><strong><span id="total-price"> </span> USD</strong></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col mb-2">
            <div class="row">
                <div class="col-sm-12  col-md-6">
                    <a href="/" class="btn btn-block btn-light">Continue Shopping</a>
                </div>
                <div class="col-sm-12 col-md-6 text-right">
                    <button id="checkout" class="btn btn-lg btn-block btn-success text-uppercase">Checkout</button>
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="text-light">
    <div class="container">
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" type="text/javascript"></script>

<script>

    $( document ).ready(function() {
        $("[type='number']").keypress(function (evt) {
            evt.preventDefault();
        });

        $("#shipping-option").on('change',function() {

            if ($(this).val() === 'pickup')
            {
                $('#total-price').text($('#sub-total').text())
            }
            else if ($(this).val() === 'ups')
            {
                $('#total-price').text( parseFloat($('#sub-total').text()) + 5 )
            }
            else
            {
                $('#total-price').text('');
            }
        });

        $('.delete-item').on('click', function() {
            let id = $(this).attr('data-id')
            $.ajax
            ({
                type: "DELETE",
                url: 'http://192.168.8.248:8000/api/cart/delete/' + id,
                contentType: "application/json",
                dataType: 'json',
                async: false,
                success: function (data) {
                    console.log(data);
                    location.reload();
                },
                error(Request, status, error) {
                    console.log(error);
                }
            })
        })

        $('.quantity').on('change', function() {
            let id = parseInt($(this).attr('data-id'))
            let quantity = parseInt($(this).val())

            $.ajax
            ({
                type: "POST",
                url: 'http://192.168.8.248:8000/api/cart/update/' + id,
                contentType: "application/json",
                dataType: 'json',
                async: false,
                data: JSON.stringify({quantity: quantity}),
                success: function (data) {
                    console.log(data);
                    location.reload();
                },
                error(Request, status, error) {
                    console.log(error);
                }
            })

        })

        $('#checkout').on('click', function() {

            if(parseFloat($('#sub-total').text()) === 0)
            {
                alert('your cart is empty')
                return
            }

            if ($('#shipping-option').val() === 'null')
            {
                alert('Please select shipping mode');
                return;
            }

            $.ajax
            ({
                type: "POST",
                url: 'http://192.168.8.248:8000/api/cart/pay',
                contentType: "application/json",
                dataType: 'json',
                async: false,
                data: JSON.stringify({shipping_option: $('#shipping-option').val()}),
                success: function (data) {
                    console.log(data);

                    if (data.status === 'nok')
                    {
                        alert('You do not have enough money in your wallet')

                    }
                    else
                    {
                        alert('Thank you for choosing our Shop')
                        window.location.href ='/';
                    }
                },
                error(Request, status, error) {
                    console.log(error);
                }
            })

        })

    });

</script>

</body>
</html>