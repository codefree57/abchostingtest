<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="container">
        <a class="navbar-brand" href="/">AbcTest</a>

        <div class="collapse navbar-collapse justify-content-end" id="navbarsExampleDefault">
            <a class="btn btn-success btn-sm ml-3" href="#1">
                <i class="fa fa-money"></i>
                <span id="wallet"><?php echo $compact['session']->get('wallet') ?></span>
                <span class="badge badge-light">USD</span>
            </a>
            <a class="btn btn-success btn-sm ml-3" href="/cart">
                <i class="fa fa-shopping-cart"></i> Cart
                <span class="badge badge-light" id="cart"> <?php echo $compact['session']->get('items_count') ?> </span>
            </a>
        </div>
    </div>
</nav>

<div class="container mt-3">
    <div class="row">
        <div class="col-sm">
            <div class="card">
                <div class="card-header bg-primary text-white text-uppercase">
                    <i class="fa fa-star"></i>products *Please click in *rating* to rate
                </div>
                <div class="card-body">
                    <div class="row">

                <?php
                $products = $compact['products'];
                foreach ($products as $product) {
                    $id = $product['id'];
                    $name = $product['name'];
                    $price = $product['price'];
                    $image = $product['image'];
                    $rate = $product['avg'] ? round($product['avg'],1) : 'N/A';
                    echo <<<HEREDOC
                <div class="col-sm">
                    <div class="card">
                        <img class="card-img-top" src="$image" alt="Image">
                        <div class="card-body">
                            <h4 class="card-title"><a href="product.html" title="View Product">$name</a></h4>
                            <div class="row">
                                <div class="col">
                                    <p class="btn btn-danger btn-block">$price USD</p>
                                </div>
                                <div class="col">
                                    <p class="rate btn btn-danger btn-block" data-id="$id">Rating: $rate</p>
                                </div>
                                <div class="col">
                                    <a href="#1" class="btn btn-success btn-block add-product" 
                                        data-id="$id" data-name="$name" data-price="$price" data-image="$image">
                                        Add to cart
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
HEREDOC;
                }
                ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="text-light">
    <div class="container">
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" type="text/javascript"></script>

<script>
    $( document ).ready(function() {

        $(".add-product").on('click',function() {

            let data = {
                id: $(this).attr('data-id'),
                name: $(this).attr('data-name'),
                price: $(this).attr('data-price'),
                image: $(this).attr('data-image'),
            }

            $.ajax
            ({
                type: "POST",
                url: 'http://192.168.8.248:8000/api/cart/store',
                contentType: "application/json",
                dataType: 'json',
                async: false,
                data: JSON.stringify(data),
                success: function (data) {
                    console.log(data);
                    location.reload();
                },
                error(Request, status, error) {
                    console.log(error);
                }
            })
        })

        $('.rate').on('click', function() {
            let values = ['1', '2', '3', '4', '5']
            let rate = prompt("Please enter a value from 1 to 5");

            if ( values.includes(rate) === false )
            {
                alert('Please enter a right value from 1 to 5')
                return
            }

            let data = {
                product_id: parseInt($(this).attr('data-id')),
                value: parseInt(rate),
            }

            $.ajax
            ({
                type: "POST",
                url: 'http://192.168.8.248:8000/api/product/rate',
                contentType: "application/json",
                dataType: 'json',
                async: false,
                data: JSON.stringify(data),
                success: function (data) {
                    console.log(data);
                    if (data.status === 'nok')
                    {
                        alert('Already done you can not rate twice')
                        return
                    }
                    location.reload();
                },
                error(Request, status, error) {
                    console.log(error);
                }
            })
        })
    })
</script>

</body>
</html>